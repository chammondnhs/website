<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<?php include 'head.php'; ?>
</head>

<body>
<div id="container">
		<?php include 'title.php'; ?>  
        
        <div id="menu">
        	<?php include 'menu.php'; ?>
        </div>
        

		<div id="content">
        
        
			<div id="content_top"></div>
			<div id="content_main">
				<h2>A little bit about me.</h2>
				<p>&nbsp;</p>
				<p>&nbsp;</p>
				<h3>High School</h3>
					<p>I went to Northern York High School in Dillsburg Pennsylvania. My favorite classes were math and science because those are most natural for me.
						I had a small group of friends which I still talk to on a regular basis.</p>
					<p>&nbsp;</p>
				<h3>Sports</h3>
					<p>Although I was no all star I was pretty athletic. I enjoyed playing soccer and track. My favorite position in soccer was defense, because my mindset
						likes to wait for opportunities then capatilze on them. Some coaches just do not understand though. I was involved with all of the jumping in track.
						My favorite jump was triple jump although I was best at high jump which is ironic for my height. Eric I will still beat you in triple jump challenge
						anyday anywhere anytime!</p>
					<p>&nbsp;</p>
				<h3>College</h3>
					<p>I am currently attending Pennsylvania State University for Computer Engineering. I originally planned on going for nuclear engineering and designing reactors,
						 however Fuckushima happened. I also found an awesome club called SSPL which is designing a satellite and got interested in electronics. After doing some more
						 research I realized that being a computer engineer would allow me to go into almost any industry. Although some of my peers had a headstart on me, I quickly
						 caught up and am now at the top of the class. I have had very little electrical engineering experience but my programming experience is very extensive.</p>
					<p></p>
					<p>&nbsp;</p>
				<h3>Ambition</h3>
					<p>My ambition lies in exploring space. Although to be honest with you, working with any type of robotic system is fun. Idealy I would like to be a systems engineer
					for a rover or other space vehicle. I find asteriod mining very intriging and would like to be on a team for this. Not very many companies are trying this,
					however I feel like there is a lot of money that could be made from this. The control algorithms to land on an asteriod would have to be very complex and robust(
					my specialty)!</p>
				</div>
        <div id="content_bottom"></div>
            
      </div>
   </div>
</body>
</html>
