<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<?php include 'head.php'; ?>
</head>

<body>
<div id="container">
		<?php include 'title.php'; ?> 
        
        <div id="menu">
        	<?php include 'menu.php'; ?>
        </div>
     
		<?php include 'Personal-Menu.php'; ?>
		
	<div id="content">
		<div id="content_top"></div>
		<div id="content_main">
			<h2> These are just a few of my professional qualities </h2>
				<p>&nbsp;</p>
				<p>&nbsp;</p>
			<h3> Leadership </h3>
				<p> I am an Eagle Scout who enjoys the great outdoors and high adventure trips. It is on these trips that I have gained most of my leadership skills. </p>
				<p>&nbsp;</p>
			<h3> Programming Experience </h3>
				<p> I have extensive programming experience which can be seen from the projects that I have completed.</p>
				<p>&nbsp;</p>
			<h3> Digital Design </h3>
				<p> I have a basic understanding of designing pcb boards including schematics and board layout.</p>
				<p>&nbsp;</p>
		</div>
		<div id="content_bottom"></div>
		<?php include 'footer.php'; ?>
		</div>
	</div>
</body>
</html>
