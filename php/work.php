<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<?php include 'head.php'; ?>
</head>

<body>
<div id="container">
		<?php include 'title.php'; ?> 
        
        <div id="menu">
        	<?php include 'menu.php'; ?>
        </div>
           
		<?php include 'Work-Menu.php'; ?>
		
		<div id="content">
            
        <div id="content_top"></div>
        <div id="content_main">
        	<h2>The Projects that I am currently working on! Or have worked on</h2>
				<p>&nbsp;</p>
				<p>&nbsp;</p>
			<h3>Over View</h3>
				<p>Most of these projects are simply ideas that I would like to try at some point in time. Feel free to click on the side links to learn more about the idea
				or contact me for more information.</p>
				<p>&nbsp;</p>
			<h3>OSIRIS</h3>
				<p>Osiris is a cube sat that will hopefully be orbiting earth within a few years. I am the command and data handling lead for the satellite so I have an understanding of the entire
				system. Leading the software development requires knowledge of how every system interacts with each other.</p>
				<p>&nbsp;</p>
			<h3>Recursive Transmission Protocol (RTP)</h3>
				<p>RTP is a simple transmission protocol that I would like to try to develop. As you guessed it this protocol will utilize the power of recursion to reduce overhead durring transmission.</p>
				<p>&nbsp;</p>
			<h3>HTTP Server</h3>
				<p>This is a java program that allows users to control access to uploaded files. It is meant to be implemented like drop-box or google drive however be completely local.
				The strength of the program lies in the adminstrator(the person running the server) to control all access to files uploaded.</p>
				<p>&nbsp;</p>
        	<h3>Web Server</h3>
				<p>Many web servers have been created, however this idea is unique. I came up with this idea after helping design a webserver in java with a friend. I realized that optimizing cpu cycles 
				is similar to optimizing memory allocation and decided to use this idea to program a new server.</p>
			
          <p></p>
			<p>&nbsp;</p>
        </div>
        <div id="content_bottom"></div>
        <?php include 'footer.php'; ?>    
      </div>
   </div>
</body>
</html>
