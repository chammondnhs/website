<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<?php include 'head.php'; ?>
</head>

<body>
<div id="container">
	<?php include 'title.php'; ?>  
	
	<div id="menu">
		<?php include 'menu.php'; ?>
	</div>
	
	<?php include 'Personal-Menu.php'; ?>
		
	<div id="content">
		<div id="content_top"></div>
		<div id="content_main">
			<h2> Digital Design	</h2>
			<p>&nbsp;</p>
			<p>&nbsp;</p>
			<h3> Overview </h3>
			<p>I have used Altium Designer as well as KiCad for schematic and board layout. Altium is a much better tool although it is quite a bit more expensive.</p>
			<p>&nbsp;</p>
			<p>I used Altium to help create schematics for a rockets payload. This payload collected atmospheric data and transmitted the data to a ground station.
				Although we did not use Altium to generate Gerber files to create a pcb board we did use it to layout where we should run wire for the perfboard soldering.
				I am also currently using Altium to help finish board layout of the CDH board on OSIRIS.</p>
			<p>&nbsp;</p>
			<p>I used KiCad on a few personal projects to learn the basics of schematic setup and board layout. I also used it to turn a pdf into gerber files for ForeInnovations.
				This involved schematic layout, net lists and footprints, as well as board layout. The interesting thing about this project was the communication with pcb manufacturers
				to understand their limitations while creating pcb boards.</p>
			<p>&nbsp;</p>
			<h3> Near Future </h3>
			<p>I am currently taking comp eng 270 which is digital design. This will teach me a basics of gate logic and how transistors and other ic circuits work. The other digital design
				class I am taking is EE210. This is about circuits and devices which will provide more of a background into digital engineering. These classes will be completed by summer of 2013</p>
		</div>
		<div id="content_bottom"></div>
		<?php include 'footer.php'; ?>
		</div>
	</div>
</body>
</html>
