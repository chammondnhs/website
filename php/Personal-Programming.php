<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<?php include 'head.php'; ?>
</head>

<body>
<div id="container">
	<?php include 'title.php'; ?>  
	
	<div id="menu">
		<?php include 'menu.php'; ?>
	</div>
	
	<?php include 'Personal-Menu.php'; ?>
		
	<div id="content">
		<div id="content_top"></div>
		<div id="content_main">
			<h2> Programming	</h2>
			<p>&nbsp;</p>
			<p>&nbsp;</p>
			<h3> Strong Suit </h3>
			<p>I have extensive experience with C++ and Java. These are both very essential languages and have their pros and cons. I do have experience with other languages
				however I prefer these two. I have developed some visual basic scripts and batch scripts for Highmark, however these were primarily created revising previously
				created code. For Osiris I have also programmed in C and C#. We use C for the interprocess communication and some drivers. I used C# with the help of a friend to create
				a testing procedure for measuring drift time of the real time clock and correcting it.</p>
			<p>&nbsp;</p>
			<h3> HTML </h3>
			<p>You are reading this website on basic HTML/XHTML and PHP. The HTML ideas were taken from the template maker and improved. I used his CSS becuase I did not feel like taking 
				the time to create my own. The PHP resulted becuase I hate copying and pasting code. Since the server is pretty small I develope the html and php on my personal 
				desktop thus offloading the php work to my desktop rather than the server.</p>
			<p>&nbsp;</p>
			<h3> Operating Systems </h3>
			<p>	I have familiarity with UNIX/LINUX. My personal computer is duel booted with windows 7 and Ubuntu. I participated in the beta version of windows 8 on my laptop, which
				will be switched to Ubuntu when the trial runs out. I also use cygwin with windows 7 occasionally. This server is running arch linux. I am familiar with git and use it 
				for version control on a few of my projects. During my Internship at Highmark I helped fix windows XP machines.</p>
			<p>&nbsp;</p>
			<h3> Result </h3>
			<p> I can program almost anything and am willing to learn any language to accomplish the end result. I have a flexible software background that can be adapted and added
				on to for any future project. I also have used most current operating systems so I have the ability to code in any environment.</p>
		</div>
		<div id="content_bottom"></div>
		<?php include 'footer.php'; ?>
		</div>
	</div>
</body>
</html>
