<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<?php include 'head.php'; ?>
</head>

<body>
<div id="container">
		<?php include 'title.php'; ?>  
        
        <div id="menu">
        	<?php include 'menu.php'; ?>
        </div>
            
		<?php include 'Projects-Menu.php'; ?>
		
		<div id="content">
        
		
        
        <div id="content_top"></div>
        <div id="content_main">
        	<h2>Recursive Transmission Protocol</h2>
				<p>&nbsp;</p>
				<p>&nbsp;</p>
				<p>RTP will hopefully replace TCP and UDP someday. I think that it is very elegant and simple although the programming that needs to be done is very complex. Here is my idea!
					A data packet will contain all of the information that it needs to get to where ever it would like to go. The first set of bytes will be the control bytes which determine location,
					size, encryption and such ... and the following bytes will be the data in that packet. However what could make this very powerful is that the first packet's control bytes 
					hold information as to where it is going, while the rest of that packet will be able to hold data. It is kinda hard to explain how it works but I will be revisiting this as the kinks
					get worked out and I learn more about TCP. I just feel like TCP can and needs to be improved. Since recursion is the most powerful force in the universe why not exploit it.</p>
				<p>&nbsp;</p>
          <p></p>
			<p>&nbsp;</p>
        </div>
        <div id="content_bottom"></div>
		<?php include 'footer.php'; ?>
            
      </div>
   </div>
</body>
</html>
