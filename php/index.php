<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<?php include 'head.php'; ?>
</head>

<body>
<div id="container">
		<?php include 'title.php'; ?>   
        
        <div id="menu">
        	<?php include 'menu.php'; ?>
        </div>
        
        <div id="leftmenu">
			<div id="leftmenu_top"></div>
				<div id="leftmenu_main">              
					<h3>Links</h3>                        
						<ul>
							<li><a href="http://192.168.1.1">Internal</a></li>
							<li><a href="http://freedns.afraid.org">FreeDNS</a></li>
							<li><a href="http://www.bryantsmith.com">Template Maker</a></li>
              <li><a href="/adam/form/form.htm">MIDD PENN GAMING</a></li>
						</ul>
				</div>
                                
            <div id="leftmenu_bottom"></div>
        </div>
 
		<div id="content">      
			<div id="content_top"></div>
			<div id="content_main">
				<h2>I am a computer engineer that enjoys both programming and digital hardware design </h2>
					<p>&nbsp;</p>
					<p>&nbsp;</p>
					<img src="images/craig.jpg" alt="picture" width="25%" height="25%">
				<h3>This is me</h3>
					<p> If I had to say that I was good at one thing ... it would have to be programming. I enjoy developing algorithms and watching how data collection 
						techniques work. Someday I hope to write my own operating system. 
					</p>
					<p>&nbsp;</p>
				<h3>Why</h3>
					<p>This site was created as a by-product of an adventure. I wanted to set up a simple server in my house that would be accessible from outside the network.
					With the help of a friend I converted a GoFlexNet device into a webserver. The entire server and website was set up for less than 100 dollars. I wanted to make a 
					personal website, however the cost of storage on domains is unreasonably expensive. I can put anything on this server and will always know its status. I can also 
					change the size and setup of the server at my convenience. I appologize for typos and the incompleteness of the site. This is a work in progress as I have time.</p>
					<p>&nbsp;</p>
				<h3>Thank you</h3>
					<p> Thank you <a href="http://freedns.afraid.org"> FreeDNS </a> for allowing me to use a free subdomain to host this site. Another thank you goes out to 
					<a href="http://www.bryantsmith.com"> Bryant Smith </a> for creating a free template. Although this is far from the original source code, your thoughts inspired this
					website. Thank you <a href="http://nginx.org">Nginx</a> for allowing me to use your webserver.</p>
			</div>
			<div id="content_bottom"></div>
			<?php include 'footer.php'; ?>
        </div>       
			
</div>
</body>
</html>
