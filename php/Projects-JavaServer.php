<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<?php include 'head.php'; ?>
</head>

<body>
<div id="container">
		<?php include 'title.php'; ?> 
        
        <div id="menu">
        	<?php include 'menu.php'; ?>
        </div>
            
		<?php include 'Projects-Menu.php'; ?>
		<div id="content">
        
		
        
        <div id="content_top"></div>
        <div id="content_main">
        	<h2>HTTP Server</h2>
				<p>&nbsp;</p>
				<p>&nbsp;</p>
				<p>As a final project for computer science 221(Java) a friend and I programmed a http server. This acts more as a dropbox where you can run a server
					and control access to files that have been uploaded. What makes this powerful is that the person running the server has complete control over the 
					server and all of the files uploaded. The interface to upload files is html based with javascript to interact with the running server. The server
					itself is programmed in Java. To modify configurations of the server I created a java gui which interacts with a sql database. The entire system 
					can be completely secure. The passwords are stored as a salted sha512 hash with no implemented decoding algoritm. If enabled the logon to the server
					can be secured through ssl.</p>
				<p>&nbsp;</p>
				<p>&nbsp;</p>
        </div>
        <div id="content_bottom"></div>
		<?php include 'footer.php'; ?>
            
      </div>
   </div>
</body>
</html>
