<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<?php include 'head.php'; ?>
</head>

<body>
<div id="container">
		<?php include 'title.php'; ?> 
        
        <div id="menu">
        	<?php include 'menu.php'; ?>
        </div>
            
		<?php include 'Work-Menu.php'; ?>
		<div id="content">
        
		
        
        <div id="content_top"></div>
        <div id="content_main">
        	<h2>SSPL</h2>
				<p>&nbsp;</p>
				<p>&nbsp;</p>
				<p>I am the CDH lead for SSPL. This primarily means leading meetings to discuss what needs done and coordinate with people to make sure these things get done. During the 
				leads meeting I interact with the leads of the other subsystems and we discuss issues that we are having combining our efforts on the satellite. I am also involved
				with proposals to try to get funding for our system.</p>
				<p>&nbsp;</p>
				<p>&nbsp;</p>
        </div>
        <div id="content_bottom"></div>
		<?php include 'footer.php'; ?>
            
      </div>
   </div>
</body>
</html>
