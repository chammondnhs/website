<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<?php include 'head.php'; ?>
</head>

<body>
<div id="container">
	<?php include 'title.php'; ?>  
	
	<div id="menu">
		<?php include 'menu.php'; ?>
	</div>
	
	<?php include 'Personal-Menu.php'; ?>
		
	<div id="content">
		<div id="content_top"></div>
		<div id="content_main">
			<h2> Leadership	</h2>
			<p>&nbsp;</p>
			<p>&nbsp;</p>
			<h3> Overview </h3>
			<p>I am an Eagle Scout who enjoys the great outdoors and high adventure trips. It is on these trips that I have gained most of my leadership skills.
			Nothing can bring out the best and worst of a group of people than a trip that could kill all of you. Problem resolution is essential to maintaining
			stability within a group. Being a friend while still accomplishing objectives is a very hard struggle. </p>
			<p>&nbsp;</p>
			<h3>Northern Tier</h3>
			<p>I lead a group of high adventure boyscouts on a 75 mile canoe trip through the backwoods of canada. We were in quetico provincial park and did not
			see very many people other than each other during our trip. We were given a guide to help us through the trip, becuase of the danager involved. The problem
			that arose however is that two leaders then emerged, me and him. This did not become a problem until the guide put our entire group in danager and I was scared
			that under his leadership someone would be hurt. I confronted him with my resolution and the rest of the trip was completed very successfully.</p>
			<p>&nbsp;</p>
			<p>If someones favorite fish is pike what do you think of them? Becuase I discovered that I loved the taste of pike on this trip. During one of our nights I caught
			a pike and decided to create a fire(with flint and steel) then roast this fish over it. I found a cherry limb and roasted the fish to perfection. The circumstances
			might be what made the fish taste so good, but regardless that is an experience I will never forget.</p>
		</div>
		<div id="content_bottom"></div>
		<?php include 'footer.php'; ?>

		</div>
	</div>
</body>
</html>
