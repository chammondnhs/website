<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<?php include 'head.php'; ?>
</head>

<body>
<div id="container">
		<?php include 'title.php'; ?> 
        
        <div id="menu">
        	<?php include 'menu.php'; ?>
        </div>
          
		<?php include 'Projects-Menu.php'; ?>
		
		<div id="content">
        
		
        
        <div id="content_top"></div>
        <div id="content_main">
        	<h2>Web Server </h2>
				<p>&nbsp;</p>
				<p>&nbsp;</p>
				<p>I know that this is not a new idea because there are thousands of web servers. However, they are all programmed a little differently. Some spawn a thread for each request while
				some spawn a new process for each request. Some handle the requests asyncriously and others do not. How is mine going to be different? Think of memory allocation and storage containers
				in programming languages. A good container is very fast but very flexible. I was planning on modeling the server after this concept. It will be a process running with lets say 10 threads
				that are ready to handle requests. However, if more requests come in then the process allocates more threads and sends the requests to the threads. If the number of requests drop off
				and stay relatively low then some of the threads can be released. The power in this is that new threads(or processes doesn't really matter) are not spawned and killed for each new request.
				Extra threads are spawned only if needed and they are kept alive as long as they might be needed. So there will always be buffer threads ready to catch incoming requests. This could possibly
				tax the memory a little bit harder than other types of servers, however the number of clock cycles it takes to handle a request will be drastically reduced. This should improve overall speed 
				of the server.</p>
          <p></p>
			<p>&nbsp;</p>
        </div>
        <div id="content_bottom"></div>
		<?php include 'footer.php'; ?>

      </div>
   </div>
</body>
</html>
