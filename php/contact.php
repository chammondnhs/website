<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<?php include 'head.php'; ?>
</head>

<body>
<div id="container">
		<?php include 'title.php'; ?> 
        
        <div id="menu">
        	<?php include 'menu.php'; ?>
        </div>
          
		<div id="content">      
        
			<div id="content_top"></div>
			<div id="content_main">
				<h2>Feel free to contact me!</h2>
					<p>I enjoy talking about projects that I have worked on as well as programming techniques. I appologize if it takes me 
						a long time to respond. I am not very involved with social media sites. However, emails will be responded to within a day or two!</p>
					<p>&nbsp;</p>
					<p>&nbsp;</p>
				<h3>FaceBook</h3>
					<p> <a href="http://www.facebook.com/craig.hammond.31" target="_blank">Craig Facebook</a></p>
					<p>&nbsp;</p>
				<h3>Linked In</h3>
					<p> <a href="http://www.linkedin.com/pub/craig-hammond/62/304/a5b" target="_blank">Craig Linked In</a></p>
					<p>&nbsp;</p>
					
				<h3>Email addresses</h3>
					<p> chammondnhs@gmail.com </p>
					<p> cmh5701@psu.edu </p>
					<p>&nbsp;</p>
			</div>
			<div id="content_bottom"></div>
            
		</div>
</div>			
</body>
</html>
