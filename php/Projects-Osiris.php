<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<?php include 'head.php'; ?>
</head>

<body>
<div id="container">
		<?php include 'title.php'; ?> 
        
        <div id="menu">
        	<?php include 'menu.php'; ?>
        </div>
            
		<?php include 'Projects-Menu.php'; ?>
		<div id="content">
        
		
        
        <div id="content_top"></div>
        <div id="content_main">
        	<h2>OSIRIS</h2>
				<p>&nbsp;</p>
				<p>&nbsp;</p>
				<p>This is a satellite that will hopefully be orbiting earth in a few years. I am the Command and Data Handling lead for this project. I primarily work with the flight software, 
					however since I am also involved with the hardware I know must of what is on the satellite and how it works. The software is developed completely in house (although we do 
					exploit free software as much as we can). The data collection algorithm is a unique combination of processes, threads, and inter process communication to deliver the best performance.
					If you would like more details on the project or would like to help please let me know! We can use every type of programmer although the flight software is written in C and C++.</p>
				<p>&nbsp;</p>
				<p>&nbsp;</p>
        </div>
        <div id="content_bottom"></div>
		<?php include 'footer.php'; ?>
            
      </div>
   </div>
</body>
</html>
